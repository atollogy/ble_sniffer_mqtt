#! /usr/bin/env node

// Set advertisement rate to be once every five seconds. This must be set before
// including the bleno library (which is included from eddystone-beacon)
process.env['BLENO_ADVERTISING_INTERVAL'] = 5000;

var debug = require('debug')('gw_beacon');
var eddystoneBeacon = require('eddystone-beacon');
var os = require('os');

var jfs = require('jsonfile');
var bcm = jfs.readFileSync('/etc/kvmanager/bcm.json');

var gatewayId = bcm.gateway_id && /^[0-9A-Fa-f]{12}$/.test(bcm.gateway_id) ? bcm.gateway_id.toLowerCase() : "000000000000";

var ATOLLOGY_GATEWAY_UUID = "ad135bf708d24171a14aeecc08752f96";
var GATEWAY_NAMESPACE = ATOLLOGY_GATEWAY_UUID.slice(0,8) + ATOLLOGY_GATEWAY_UUID.slice(20); //ad135bf7eecc08752f96

function getTruncatedHostname(maxSize){
    var hostname = os.hostname();
    var idx = hostname.indexOf(".");
    return (idx > 0) ? hostname.substring(0,Math.min(idx, maxSize)) : // use up to maxSize characters before the first dot
           (hostname.length > maxSize) ? hostname.substring(0,maxSize) : // if no dash, use up to maxSize characters
           hostname
}

function getAddress(){
    var interfaces = os.networkInterfaces();
    var addressHash = {};
    for (var interfaceKey in interfaces){
        var entries = interfaces[interfaceKey].filter(function(entry){ return entry.family == "IPv4" && !entry.internal; });
        if(entries.length){
            addressHash[interfaceKey] = entries[0].address;
        }
    }

    var interfacesFound = Object.keys(addressHash);

    //prefer wlan0, but otherwise take the first one we find
    return addressHash.wlan0 || ((interfacesFound.length) ? addressHash[interfacesFound[0]] : undefined);
}

var lastAddress = getAddress();
var currentlyAdvertising = false;

function updateAdvertisement(addr){
    if(!addr || !gatewayId) return;

    var ble_adv_options = {
        name: getTruncatedHostname(28 - addr.length) + ":" + addr,
        txPowerLevel: -10
    };

    if(currentlyAdvertising){
        eddystoneBeacon.stop();
    }

    eddystoneBeacon.advertiseUid(GATEWAY_NAMESPACE, gatewayId, ble_adv_options);
    currentlyAdvertising = true;
    console.log(`Advertising with namespace: ${GATEWAY_NAMESPACE}, id: ${gatewayId}`);
}

updateAdvertisement(lastAddress);

function startAdvertising(){
    debug('starting_ble_advertisements');

    setInterval(function(){
        var newAddress = getAddress();
        if (newAddress != lastAddress){
            lastAddress = newAddress;
            updateAdvertisement(lastAddress);
        }
    }, 5*60*1000);
}


module.exports = {
    startAdvertising: startAdvertising
}