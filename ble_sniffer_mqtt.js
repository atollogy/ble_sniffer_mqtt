#!/usr/bin/env node

//******************************************************************************
// Publish the MAC addresses of all sniffed BLE packets.
//******************************************************************************
process.env['BLENO_ADVERTISING_INTERVAL'] = 5000;

var debug = require('debug')('ble_sniffer_mqtt');
var BeaconScanner = require("./beaconScanner");
var exec = require('child_process').exec;
var gwBeacon = require("./gw_beacon");
var jfs = require('jsonfile');
var os = require('os');

//******************************************************************************
// Load configuration
//******************************************************************************

var bcm = jfs.readFileSync('/etc/kvmanager/bcm.json');

//gateway id must be a six byte value represented in hex
var gateway_id = bcm.gateway_id && /^[0-9A-Fa-f]{12}$/.test(bcm.gateway_id) ? bcm.gateway_id.toLowerCase() : undefined;

if (!gateway_id){
    console.warn("missing or invalid gateway id at bluetooth sniffer startup");
}

// MQTT
var mqtt = require('mqtt');
var _mqtt_client = mqtt.connect(
    bcm.svcs[bcm.scode].mqtt_server,
    bcm.svcs[bcm.scode].mqtt_port
);

//******************************************************************************
// CONFIGURATION OPTIONS
//******************************************************************************

// MQTT Topics
var ATL_PREFIX = `atollogy/ble/${bcm.shost}`;

//******************************************************************************
// VALID BEACON LIST
//******************************************************************************

function UuidToEddystoneNamespace(uuid){
    return uuid.slice(0,8) + uuid.slice(20);
}

const KONTAKT_UUID = 'f7826da64fa24e988024bc5b71e0893e';
const ESTIMOTE_UUID = 'b9407f30f5f8466eaff925556b57fe6d'; //not currently used
const ESTIMOTE_EDDYSTONE_NAMESPACE = 'edd1ebeac04e5defa017';      //not currently used

const ATOLLOGY_GATEWAY_UUID = 'ad135bf708d24171a14aeecc08752f96';
const ATOLLOGY_BEACON_UUID =  'aba254642a9648ebac11175f57e0d3b1';
const VALID_UUIDS = [ATOLLOGY_BEACON_UUID, ATOLLOGY_GATEWAY_UUID, KONTAKT_UUID];
const VALID_EDDYSTONE_NAMESPACES = VALID_UUIDS.map(UuidToEddystoneNamespace);

//******************************************************************************
// MAIN CODE
//******************************************************************************

var GATEWAY_NAMESPACE = UuidToEddystoneNamespace(ATOLLOGY_GATEWAY_UUID);  //ad135bf7eecc08752f96

function makeAtlBeacon (beacon) {
    //prepare discovered beacon for upload
    var now = (new Date()).getTime();

    beacon.gateway_id = gateway_id;
    beacon.nodename = bcm.nodename;
    beacon.provenance = [beacon.namespace +":"+ beacon.instance, GATEWAY_NAMESPACE +":"+ beacon.gateway_id];

    beacon.lastSeen = Math.floor(now/1000.0);

    return beacon;
}

function isValidBeacon(frame){
    if(!frame) return false;
    if (frame.beacon_type == 'iBeacon'){
        return VALID_UUIDS.includes(frame.namespace);
    } else if (frame.beacon_subtype == 'uid'){
        return VALID_EDDYSTONE_NAMESPACES.includes(frame.namespace);
    }
    return false;
}

_mqtt_client.on("connect", function(){
    console.log("BLE sniffer MQTT client connected");
});

BeaconScanner.on('discover', function(frame){
    if (isValidBeacon(frame)){
        var beacon = makeAtlBeacon(frame);
        _mqtt_client.publish(ATL_PREFIX, JSON.stringify(beacon));
    }
});

gwBeacon.startAdvertising();
BeaconScanner.startScanning();

console.log("BLE sniffer started");
