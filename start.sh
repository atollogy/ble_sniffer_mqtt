#!/usr/bin/env bash

cd /opt/ble_sniffer_mqtt
while [ $(hciconfig hci0| grep -c UP) -lt 1 ]
do
   sleep 3
   echo "$(date) $(hostname) ble_sniffer_starter: attemptiing to bring up hci0 - $(sudo /bin/hciconfig hci0 up)" >> /var/log/syslog
done
/usr/bin/node ./ble_sniffer_mqtt.js 
