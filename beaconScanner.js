// force reporting all HCI events on Linux
process.env['NOBLE_REPORT_ALL_HCI_EVENTS'] = 1;

var events = require('events');
var util = require('util');
var noble = require('noble');

var urlDecode = require('eddystone-url-encoding').decode;

var debug = require('debug')('BeaconScanner');

//iBeacon configuration info
var EXPECTED_IBEACON_MANUFACTURER_DATA_LENGTH = 25;
var APPLE_COMPANY_IDENTIFIER = 0x004c; // https://www.bluetooth.org/en-us/specification/assigned-numbers/company-identifiers
var IBEACON_TYPE = 0x02;
var EXPECTED_IBEACON_DATA_LENGTH = 0x15;

//Eddystone configuration info
var EDDYSTONE_SERVICE_UUID = 'feaa';
var EDDYSTONE_UID_FRAME_TYPE = 0x00;
var EDDYSTONE_URL_FRAME_TYPE = 0x10;
var EDDYSTONE_TLM_FRAME_TYPE = 0x20;

function isIBeacon(advertisement){
    var manufacturerData = advertisement.manufacturerData;
    return manufacturerData &&
           EXPECTED_IBEACON_MANUFACTURER_DATA_LENGTH <= manufacturerData.length &&
           APPLE_COMPANY_IDENTIFIER === manufacturerData.readUInt16LE(0) &&
           IBEACON_TYPE === manufacturerData.readUInt8(2) &&
           EXPECTED_IBEACON_DATA_LENGTH === manufacturerData.readUInt8(3);
}

function isEddystoneBeacon(advertisement){
    var serviceData = advertisement.serviceData;

    // make sure service data is present
    if (serviceData && serviceData.length > 0){
      // and that it has an entry with the expected UUID and data length
      var eddystoneEntry = serviceData.find( (entry) => { return entry.uuid ===  EDDYSTONE_SERVICE_UUID; });
      return eddystoneEntry && eddystoneEntry.data.length > 2;
    }

    return false;
}

function parseIBeacon(peripheral, advertisement){
    var manufacturerData = advertisement.manufacturerData;

    var uuid = manufacturerData.slice(4, 20).toString('hex');
    var instance = manufacturerData.slice(20, 24).toString('hex');

    return {
        beacon_type: 'iBeacon',
        beacon_subtype:'',
        mac: peripheral.id,
        namespace: uuid,
        instance: instance,
        major: manufacturerData.readUInt16BE(20),
        minor: manufacturerData.readUInt16BE(22),
        tx_power_1m: manufacturerData.readInt8(24), //measured power
        rssi: peripheral.rssi
    };
}

function parseEddystoneBeacon(peripheral, advertisement) {
  var data = advertisement.serviceData.find( (entry) => { return entry.uuid ===  EDDYSTONE_SERVICE_UUID; }).data;
  var frameType = data.readUInt8(0);

  var beacon = {};
  var type = 'unknown';

  switch (frameType) {
    case EDDYSTONE_UID_FRAME_TYPE:
      type = 'uid';
      beacon = parseEddystoneUidData(data);
      break;

    case EDDYSTONE_URL_FRAME_TYPE:
      type = 'url';
      beacon = parseEddystoneUrlData(data);
      break;

    case EDDYSTONE_TLM_FRAME_TYPE:
      type = 'tlm';
      beacon = parseEddystoneTlmData(data);
      break;

    default:
      break;
  }

  return {
    beacon_type: 'eddystone',
    beacon_subtype: type,
    mac:peripheral.id,
    namespace: beacon.namespace,
    instance: beacon.instance,
    tx_power_1m: (beacon.txPower !== undefined) ? beacon.txPower - 41 : undefined,
    rssi: peripheral.rssi,
    url: beacon.url,
    tlm_temp: (beacon.tlm !== undefined) ? beacon.tlm.temp : undefined,
    tlm_vbatt: (beacon.tlm !== undefined) ? beacon.tlm.vbatt : undefined,
  };
}

function parseEddystoneUidData(data) {
  return {
    txPower: data.readInt8(1),
    namespace: data.slice(2, 12).toString('hex'),
    instance: data.slice(12, 18).toString('hex')
  };
};

function parseEddystoneUrlData(data) {
  return {
    txPower: data.readInt8(1),
    url: urlDecode(data.slice(2))
  };
};

function parseEddystoneTlmData(data) {
  return {
    tlm: {
      version: data.readUInt8(1),
      vbatt: data.readUInt16BE(2),
      temp: data.readInt16BE(4) / 256,
      advCnt: data.readUInt32BE(6),
      secCnt: data.readUInt32BE(10)
    }
  };
};

var BeaconScanner = function(){
    noble.on('discover', this.onDiscover.bind(this));
}

util.inherits(BeaconScanner, events.EventEmitter);

BeaconScanner.prototype.startScanning = function() {
  debug('startScanning');

    var startScanningOnPowerOn = function() {
        if (noble.state === 'poweredOn') {
            noble.startScanning([], true);
        } else {
              noble.once('stateChange', startScanningOnPowerOn);
        }
    };

  startScanningOnPowerOn();
};

BeaconScanner.prototype.stopScanning = function() {
    debug('stopScanning');
    noble.stopScanning();
};

BeaconScanner.prototype.onDiscover = function(peripheral, advertisement){
    debug('onDiscover (%s): %s', peripheral.id, advertisement);

    try {
      var beacon;
      if (isIBeacon(advertisement)){
          beacon = parseIBeacon(peripheral, advertisement);
      } else if (isEddystoneBeacon(advertisement)){
          beacon = parseEddystoneBeacon(peripheral, advertisement);
      }

      if (beacon){
        this.emit('discover', beacon);
      }
      
    } catch (e) {
      console.log("An error occurred parsing beacon data.");
      console.log(e);
    }
}

module.exports = new BeaconScanner();